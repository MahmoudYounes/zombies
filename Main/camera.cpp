#include "camera.h"

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define min(a, b) a > b ? b : a
#define max(a, b) a < b ? b : a

  Camera::Camera(glm::vec3 position, float angle_LR, float angle_UD, Skybox* skybox)
  {
    glfwSetMousePos(1920/2, 1080/2);
    this->position = position;
    angle_LeftRight = angle_LR;
    angle_UpDown = angle_UD;
    _skybox = skybox;
    mouseSpeed = 0.05;
  }

  void Camera::MoveForward(float distance)
  {
    position.x += std::sin(angle_LeftRight / 180 * M_PI) * distance;
    position.z -= std::cos(angle_LeftRight / 180 * M_PI) * distance;
  }

  void Camera::MoveBackward(float distance)
  {
    position.x -= std::sin(angle_LeftRight / 180 * M_PI) * distance;
    position.z += std::cos(angle_LeftRight / 180 * M_PI) * distance;
  }
  void Camera::MoveLeft(float distance)
  {
    position.x += std::sin((angle_LeftRight - 90) / 180 * M_PI) * distance;
    position.z -= std::cos((angle_LeftRight - 90) / 180 * M_PI) * distance;
  }

  void Camera::MoveRight(float distance)
  {
    position.x += std::sin((angle_LeftRight + 90) / 180 * M_PI) * distance;
    position.z -= std::cos((angle_LeftRight + 90) / 180 * M_PI) * distance;
  }

  const glm::vec3 Camera::Get_position()
  {
    return position;
  }

  float Camera::Get_LR_angle()
  {
    return angle_LeftRight;
  }

  void Camera::calculate_mouse()
  {
      // Get mouse position
      int xpos, ypos;
      glfwGetMousePos(&xpos, &ypos);
      // Reset mouse position for next frame
      glfwSetMousePos(1920/2, 1080/2);

      // Compute new orientation
      angle_LeftRight += -(mouseSpeed * float(1920/2 - xpos ));
      angle_UpDown    += -(mouseSpeed * float(1080/2 - ypos ));
      angle_UpDown = min(angle_UpDown, 45);
      angle_UpDown = max(angle_UpDown, -45);
      //angle_UpDown    = max(-45, angle_UpDown);

  }
  void Camera::Process()
  {
    calculate_mouse();

    glRotatef(angle_UpDown    , 1, 0, 0);
    glRotatef(angle_LeftRight , 0, 1, 0);

    if(_skybox)
        _skybox->Draw();

    glTranslatef(-position.x, -position.y, -position.z);
  }
  glm::vec3 Camera::GetDirection()
  {
      return glm::vec3(sin(angle_LeftRight / 180.0 * M_PI),
                       -sin(angle_UpDown / 180.0 * M_PI),
                       -cos(angle_LeftRight / 180.0 * M_PI)
                           );
  }
