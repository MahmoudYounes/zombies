#ifndef BUILDINGS_h
#define BUILDINGS_h

#include "GameObject.h"
#include "Model.h"

class Building : public GameObject
{
    Model *model;
    float minx, maxx;
    float miny, maxy;
    float minz, maxz;
    btRigidBody* CreateBody();
public:
    Building(glm::vec3, Model *model);
    void draw();
};

#endif
