#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

#include "Model.h"

class GameObject
{

public:
    btRigidBody *body;
    glm::vec3 position;
    virtual void draw() = 0;
    virtual btRigidBody* CreateBody() = 0;
};


#endif
