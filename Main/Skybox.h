#ifndef SKYBOX_H_
#define SKYBOX_H_

#include "Texture.h"

class Skybox
{
	Texture texture_top;
	Texture texture_down;
	Texture texture_front;
	Texture texture_back;
	Texture texture_left;
	Texture texture_right;

public:
	Skybox(char* top, char* down, char* front, char* back, char* left, char* right);
	void Draw();
};

#endif