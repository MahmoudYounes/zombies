#include "Skybox.h"

Skybox::Skybox(char* top, char* down, char* front, char* back, char* left, char* right)
{
	texture_top.Load  (top);
    texture_down.Load (down);
    texture_front.Load(front);
    texture_back.Load (back);
    texture_left.Load (left);
    texture_right.Load(right);
}

void Skybox::Draw()
{
    glScalef(200, 200, 200);

	// Disable lighting, that would reveal the cube
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, texture_front.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(0,0); glVertex3f(-5, -5, -5);
	    glTexCoord2f(1,0); glVertex3f( 5, -5, -5);
	    glTexCoord2f(1,1); glVertex3f( 5,  5, -5);
	    glTexCoord2f(0,1); glVertex3f(-5,  5, -5);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture_down.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(0,0); glVertex3f(-5, -5,  5);
	    glTexCoord2f(1,0); glVertex3f( 5, -5,  5);
	    glTexCoord2f(1,1); glVertex3f( 5, -5, -5);
	    glTexCoord2f(0,1); glVertex3f(-5, -5, -5);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture_back.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(1,0); glVertex3f(-5, -5, 5);
	    glTexCoord2f(1,1); glVertex3f(-5,  5, 5);
	    glTexCoord2f(0,1); glVertex3f( 5,  5, 5);
	    glTexCoord2f(0,0); glVertex3f( 5, -5, 5);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture_top.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(0,0); glVertex3f(-5, 5, -5);
	    glTexCoord2f(1,0); glVertex3f( 5, 5, -5);
	    glTexCoord2f(1,1); glVertex3f( 5, 5,  5);
	    glTexCoord2f(0,1); glVertex3f(-5, 5,  5);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture_left.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(1,0); glVertex3f(-5, -5, -5);
	    glTexCoord2f(1,1); glVertex3f(-5,  5, -5);
	    glTexCoord2f(0,1); glVertex3f(-5,  5,  5);
	    glTexCoord2f(0,0); glVertex3f(-5, -5,  5);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture_right.Get_id());
	glBegin(GL_QUADS);
	    glTexCoord2f(0,0); glVertex3f(5, -5, -5);
	    glTexCoord2f(1,0); glVertex3f(5, -5,  5);
	    glTexCoord2f(1,1); glVertex3f(5,  5,  5);
	    glTexCoord2f(0,1); glVertex3f(5,  5, -5);
	glEnd();

	//Scale back to normal size
    glScalef(1.0/200, 1.0/200, 1.0/200);

	//Re-enable lighting
	glEnable(GL_LIGHTING);
}
