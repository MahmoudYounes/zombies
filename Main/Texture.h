#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <GL/glfw.h>
#include <string>
#include <vector>
#include <map>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <iostream>

class Texture
{

public:
   Texture();
  ~Texture();
  char* Get_name();
  void Set_name(char* name);
  GLuint Get_id();
  
  GLuint Load(char* );
  
  char *name;
  GLuint id;

};

#endif
