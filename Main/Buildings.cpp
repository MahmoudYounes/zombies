#include "Buildings.h"

void Building::draw()
{
    float height = maxy - miny;

    glPushMatrix();
    glTranslatef(position.x, position.y + height / 2, position.z);
    model->Draw();
    glPopMatrix();
}

Building::Building(glm::vec3 pos, Model* model)
{
    this->model = model;
    position = pos;
    body = NULL;
    minx = model->minx;
    maxx = model->maxx;
    miny = model->miny;
    maxy = model->maxy;
    minz = model->minz;
    maxz = model->maxz;
}

btRigidBody* Building::CreateBody()
{

    float depth = maxz - minz;
    float height = maxy - miny;
    float length = maxx - minx;

    btTransform transform;

    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y + height / 2, position.z));

    btBoxShape* box = new btBoxShape(btVector3(length/2.0, height/2.0, depth/2.0));
    btVector3 inertia(0,0,0);

    btMotionState* motion = new btDefaultMotionState(transform);

    btRigidBody::btRigidBodyConstructionInfo info(0, motion, box, inertia);
    btRigidBody* body = new btRigidBody(info);
    this->body = body;
    return body;
}

