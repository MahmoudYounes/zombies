#include "Model.h"
#include <vector>
#include <sstream>
#include <iostream>
#include <cfloat>

#define T(x) triangles[(x)]
#define min(a, b) a > b ? b : a
#define max(a, b) a < b ? b : a

void chop( std::string& str )
{
    std::string whitespaces (" \t\f\v\n\r");
    size_t found = str.find_last_not_of( whitespaces );
    if ( found!=std::string::npos )
        str.erase( found+1 );
    else
        str.clear();
}

void split( char* s, char delim, std::vector<std::string>& tokens )
{
    std::string str(s);
    std::stringstream iss(str);
    std::string item;
    while ( std::getline(iss, item, delim) )
    {
        if ( item!=" " && !item.empty() )
        {
            chop(item);
            tokens.push_back(item);
        }
    }
}


Model::~Model()
{
    OBJgroup* group;
    GLuint i;

    if (pathname)
        delete[] pathname;
    if (mtllibname)
        delete[] mtllibname;
    if (vertices)
        delete[] vertices;
    if (normals)
        delete[] normals;
    if (texcoords)
        delete[] texcoords;
    if (facetnorms)
        delete[] facetnorms;
    if (triangles)
        delete[] triangles;

    if (materials)
    {
        for (i = 0; i < nummaterials; i++)
            delete[] materials[i].name;
        delete[] materials;
    }
    if (textures)
        free(textures);

    while(groups)
    {
        group = groups;
        groups = groups->next;
        delete[] group->name;
        delete[] group->triangles;
        delete group;
    }
}
Model::Model(char* filename, GLuint mode)
{
    minx = FLT_MAX;
    maxx = FLT_MIN;
    miny = FLT_MAX;
    maxy = FLT_MIN;
    minz = FLT_MAX;
    maxz = FLT_MIN;

    this->Loaded = false;
    this->pathname  = NULL;
    this->mtllibname    = NULL;
    this->numvertices   = 0;
    this->vertices    = NULL;
    this->numnormals    = 0;
    this->normals     = NULL;
    this->numtexcoords  = 0;
    this->texcoords       = NULL;
    this->numfacetnorms = 0;
    this->facetnorms    = NULL;
    this->numtriangles  = 0;
    this->triangles       = NULL;\
    this->nummaterials  = 0;
    this->materials       = NULL;
    this->numtextures  = 0;
    this->textures       = NULL;
    this->numgroups       = 0;
    this->groups      = NULL;

    if(filename)
        Load(filename, mode);
}

void Model::Load(char* filename, GLuint mode)
{
    FILE*   file;

    /* open the file */
    file = fopen(filename, "r");
    if (!file)
    {
        fprintf(stderr, "load() failed: can't open object file %s.\n", filename);
        exit(1);
    }
    
    /* allocate a new model */
    this->pathname    = strdup(filename);
    this->mtllibname    = NULL;
    this->numvertices   = 0;
    this->vertices    = NULL;
    this->numnormals    = 0;
    this->normals     = NULL;
    this->numtexcoords  = 0;
    this->texcoords       = NULL;
    this->numfacetnorms = 0;
    this->facetnorms    = NULL;
    this->numtriangles  = 0;
    this->triangles       = NULL;\
    this->nummaterials  = 0;
    this->materials       = NULL;
    this->numtextures  = 0;
    this->textures       = NULL;
    this->numgroups       = 0;
    this->groups      = NULL;
    this->mode = mode;

    /* make a first pass through the file to get a count of the number
    of vertices, normals, texcoords & triangles */
    FirstPass(file);
    /* allocate memory */
    this->vertices = (GLfloat*)malloc(sizeof(GLfloat) *
        3 * (this->numvertices + 1));
    this->triangles = (OBJtriangle*)malloc(sizeof(OBJtriangle) *
        this->numtriangles);
    if (this->numnormals) 
    {
        this->normals = (GLfloat*)malloc(sizeof(GLfloat) *
            3 * (this->numnormals + 1));
    }
    if (this->numtexcoords) 
    {
        this->texcoords = (GLfloat*)malloc(sizeof(GLfloat) *
            2 * (this->numtexcoords + 1));
    }
    
    /* rewind to beginning of file and read in the data this pass */
    rewind(file);
    
    SecondPass(file);
    
    /* close the file */
    fclose(file);

    listId_ = glGenLists(1);
    glNewList( listId_, GL_COMPILE );
   
    glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );

    Build();
  
    glEndList();

    setData();

    Loaded = true;
}

void Model::FirstPass(FILE* file)
{
    GLuint  numvertices;        /* number of vertices in model */
    GLuint  numnormals;         /* number of normals in model */
    GLuint  numtexcoords;       /* number of texcoords in model */
    GLuint  numtriangles;       /* number of triangles in model */
    OBJgroup* group;            /* current group */
    unsigned    v, n, t;
    char        buf[128];
    
    /* make a default group */
    group = AddGroup("default");
    
    numvertices = numnormals = numtexcoords = numtriangles = 0;
    while(fscanf(file, "%s", buf) != EOF) {
        switch(buf[0]) {
        case '#':               /* comment */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        case 'v':               /* v, vn, vt */
            switch(buf[1])
            {
	            case '\0':          /* vertex */
	                /* eat up rest of line */
	                fgets(buf, sizeof(buf), file);
	                numvertices++;
	                break;
	            case 'n':           /* normal */
	                /* eat up rest of line */
	                fgets(buf, sizeof(buf), file);
	                numnormals++;
	                break;
	            case 't':           /* texcoord */
	                /* eat up rest of line */
	                fgets(buf, sizeof(buf), file);
	                numtexcoords++;
	                break;
	            default:
	                printf("FirstPass(): Unknown token \"%s\".\n", buf);
	                exit(1);
	                break;
	        }
            break;
        case 'm': //mtllib
            fgets(buf, sizeof(buf), file);
            sscanf(buf, "%s %s", buf, buf);
            mtllibname = strdup(buf);
            ReadMTL(buf);
            break;
        case 'u': //usemtl
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        case 'g':               /* group */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
#if SINGLE_STRING_GROUP_NAMES
            sscanf(buf, "%s", buf);
#else
            buf[strlen(buf)-1] = '\0';  /* nuke '\n' */
#endif
            group = AddGroup(buf);
            break;
        case 'f':               /* face */
            v = n = t = 0;
            fscanf(file, "%s", buf);
            /* can be one of %d, %d//%d, %d/%d, %d/%d/%d %d//%d */
            if (strstr(buf, "//")) 
            {
                /* v//n */
                sscanf(buf, "%d//%d", &v, &n);
                fscanf(file, "%d//%d", &v, &n);
                fscanf(file, "%d//%d", &v, &n);
                numtriangles++;
                group->numtriangles++;
                while(fscanf(file, "%d//%d", &v, &n) > 0) 
                {
                    numtriangles++;
                    group->numtriangles++;
                }
            }
            else if (sscanf(buf, "%d/%d/%d", &v, &t, &n) == 3) 
            {
                /* v/t/n */
                fscanf(file, "%d/%d/%d", &v, &t, &n);
                fscanf(file, "%d/%d/%d", &v, &t, &n);
                numtriangles++;
                group->numtriangles++;
                while(fscanf(file, "%d/%d/%d", &v, &t, &n) > 0) 
                {
                    numtriangles++;
                    group->numtriangles++;
                }
            }
            else if (sscanf(buf, "%d/%d", &v, &t) == 2) 
            {
                /* v/t */
                fscanf(file, "%d/%d", &v, &t);
                fscanf(file, "%d/%d", &v, &t);
                numtriangles++;
                group->numtriangles++;
                while(fscanf(file, "%d/%d", &v, &t) > 0)
                {
                    numtriangles++;
                    group->numtriangles++;
                }
            } 
            else 
            {
                /* v */
                fscanf(file, "%d", &v);
                fscanf(file, "%d", &v);
                numtriangles++;
                group->numtriangles++;
                while(fscanf(file, "%d", &v) > 0) 
                {
                    numtriangles++;
                    group->numtriangles++;
                }
            }
            break;
            
        default:
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        }
  }
  
  /* set the stats in the model structure */
  this->numvertices  = numvertices;
  this->numnormals   = numnormals;
  this->numtexcoords = numtexcoords;
  this->numtriangles = numtriangles;
  
  /* allocate memory for the triangles in each group */
  group = this->groups;
  while(group) 
  {
      group->triangles = (GLuint*)malloc(sizeof(GLuint) * group->numtriangles);
      group->numtriangles = 0;
      group = group->next;
  }
}

void Model::SecondPass(FILE* file) 
{
    GLuint  numvertices;        /* number of vertices in model */
    GLuint  numnormals;         /* number of normals in model */
    GLuint  numtexcoords;       /* number of texcoords in model */
    GLuint  numtriangles;       /* number of triangles in model */
    GLfloat*    vertices;           /* array of vertices  */
    GLfloat*    normals;            /* array of normals */
    GLfloat*    texcoords;          /* array of texture coordinates */
    OBJgroup* group;            /* current group pointer */
    GLuint  material;           /* current material */
    GLuint  v, n, t;
    char        buf[128];
	char afis[80];
    
    /* set the pointer shortcuts */
    vertices       = this->vertices;
    normals    = this->normals;
    texcoords    = this->texcoords;
    group      = this->groups;
    
    /* on the second pass through the file, read all the data into the
    allocated arrays */
    numvertices = numnormals = numtexcoords = 1;
    numtriangles = 0;
    material = 0;
    while(fscanf(file, "%s", buf) != EOF) {
        switch(buf[0]) {
        case '#':               /* comment */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        case 'v':               /* v, vn, vt */
            switch(buf[1]) {
            case '\0':          /* vertex */
                fscanf(file, "%f %f %f", 
                    &vertices[3 * numvertices + 0], 
                    &vertices[3 * numvertices + 1], 
                    &vertices[3 * numvertices + 2]);

                minx = min(minx, vertices[3 * numvertices + 0]);
                maxx = max(maxx, vertices[3 * numvertices + 0]);
                miny = min(miny, vertices[3 * numvertices + 1]);
                maxy = max(maxy, vertices[3 * numvertices + 1]);
                minz = min(minz, vertices[3 * numvertices + 2]);
                maxz = max(maxz, vertices[3 * numvertices + 2]);

                numvertices++;
                break;
            case 'n':           /* normal */
                fscanf(file, "%f %f %f", 
                    &normals[3 * numnormals + 0],
                    &normals[3 * numnormals + 1], 
                    &normals[3 * numnormals + 2]);
                numnormals++;
                break;
            case 't':           /* texcoord */
                fscanf(file, "%f %f", 
                    &texcoords[2 * numtexcoords + 0],
                    &texcoords[2 * numtexcoords + 1]);
                numtexcoords++;
                break;
            }
            break;
        case 'u':
            fgets(buf, sizeof(buf), file);
            sscanf(buf, "%s %s", buf, buf);
            group->material = material = FindMaterial(buf);
            break;
        case 'g':               /* group */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
#if SINGLE_STRING_GROUP_NAMES
            sscanf(buf, "%s", buf);
#else
            buf[strlen(buf)-1] = '\0';  /* nuke '\n' */
#endif
            group = FindGroup(buf);
            group->material = material;
            break;
        case 'f':               /* face */
            v = n = t = 0;
			this->T(numtriangles).findex = -1; // ???
			this->T(numtriangles).vecini[0]=-1;
			this->T(numtriangles).vecini[1]=-1;
			this->T(numtriangles).vecini[2]=-1;
            fscanf(file, "%s", buf);
            /* can be one of %d, %d//%d, %d/%d, %d/%d/%d %d//%d */
            if (strstr(buf, "//")) 
            {
                /* v//n */
                sscanf(buf, "%d//%d", &v, &n);
                this->T(numtriangles).vindices[0] = v;
                this->T(numtriangles).tindices[0] = 0;
                this->T(numtriangles).nindices[0] = n;
                fscanf(file, "%d//%d", &v, &n);
                this->T(numtriangles).vindices[1] = v;
                this->T(numtriangles).tindices[1] = 0;
                this->T(numtriangles).nindices[1] = n;
                fscanf(file, "%d//%d", &v, &n);
                this->T(numtriangles).vindices[2] = v;
                this->T(numtriangles).tindices[2] = 0;
                this->T(numtriangles).nindices[2] = n;
                group->triangles[group->numtriangles++] = numtriangles;
                numtriangles++;
                while(fscanf(file, "%d//%d", &v, &n) > 0) 
                {						
                    this->T(numtriangles).vindices[0] = this->T(numtriangles-1).vindices[0];
                    this->T(numtriangles).tindices[0] = this->T(numtriangles-1).tindices[0];
                    this->T(numtriangles).nindices[0] = this->T(numtriangles-1).nindices[0];

                    this->T(numtriangles).vindices[1] = this->T(numtriangles-1).vindices[2];
                    this->T(numtriangles).tindices[1] = this->T(numtriangles-1).tindices[2];
                    this->T(numtriangles).nindices[1] = this->T(numtriangles-1).nindices[2];

                    this->T(numtriangles).vindices[2] = v;
                    this->T(numtriangles).tindices[2] = 0;
                    this->T(numtriangles).nindices[2] = n;
                    group->triangles[group->numtriangles++] = numtriangles;
                    numtriangles++;
                }
            } 
            else if (sscanf(buf, "%d/%d/%d", &v, &t, &n) == 3) 
            {
                /* v/t/n */
				
                this->T(numtriangles).vindices[0] = v;
                this->T(numtriangles).tindices[0] = t;
                this->T(numtriangles).nindices[0] = n;
                fscanf(file, "%d/%d/%d", &v, &t, &n);
                this->T(numtriangles).vindices[1] = v;
                this->T(numtriangles).tindices[1] = t;
                this->T(numtriangles).nindices[1] = n;
                fscanf(file, "%d/%d/%d", &v, &t, &n);
                this->T(numtriangles).vindices[2] = v;
                this->T(numtriangles).tindices[2] = t;
                this->T(numtriangles).nindices[2] = n;
                group->triangles[group->numtriangles++] = numtriangles;
                numtriangles++;
                while(fscanf(file, "%d/%d/%d", &v, &t, &n) > 0) 
                {
					//if (n== 181228)					
                    this->T(numtriangles).vindices[0] = this->T(numtriangles-1).vindices[0];
                    this->T(numtriangles).tindices[0] = this->T(numtriangles-1).tindices[0];
                    this->T(numtriangles).nindices[0] = this->T(numtriangles-1).nindices[0];
                    this->T(numtriangles).vindices[1] = this->T(numtriangles-1).vindices[2];
                    this->T(numtriangles).tindices[1] = this->T(numtriangles-1).tindices[2];
                    this->T(numtriangles).nindices[1] = this->T(numtriangles-1).nindices[2];
                    this->T(numtriangles).vindices[2] = v;
                    this->T(numtriangles).tindices[2] = t;
                    this->T(numtriangles).nindices[2] = n;
                    group->triangles[group->numtriangles++] = numtriangles;
                    numtriangles++;
                }
            } 
            else if (sscanf(buf, "%d/%d", &v, &t) == 2) 
            {
                /* v/t */					
                this->T(numtriangles).vindices[0] = v;
                this->T(numtriangles).tindices[0] = t;
                //this->T(numtriangles).nindices[0] = 0;
                fscanf(file, "%d/%d", &v, &t);
                this->T(numtriangles).vindices[1] = v;
                this->T(numtriangles).tindices[1] = t;
                //this->T(numtriangles).nindices[1] = 0;
                fscanf(file, "%d/%d", &v, &t);
                this->T(numtriangles).vindices[2] = v;
                this->T(numtriangles).tindices[2] = t;
                //this->T(numtriangles).nindices[2] = 0;
                group->triangles[group->numtriangles++] = numtriangles;
                numtriangles++;
                while(fscanf(file, "%d/%d", &v, &t) > 0) 
                {						
				
                    this->T(numtriangles).vindices[0] = this->T(numtriangles-1).vindices[0];
                    this->T(numtriangles).tindices[0] = this->T(numtriangles-1).tindices[0];
                    //this->T(numtriangles).nindices[0] = this->T(numtriangles-1).nindices[0];

                    this->T(numtriangles).vindices[1] = this->T(numtriangles-1).vindices[2];
                    this->T(numtriangles).tindices[1] = this->T(numtriangles-1).tindices[2];
                    //this->T(numtriangles).nindices[1] = this->T(numtriangles-1).nindices[2];

                    this->T(numtriangles).vindices[2] = v;
                    this->T(numtriangles).tindices[2] = t;
                    //this->T(numtriangles).nindices[2] = 0;

                    group->triangles[group->numtriangles++] = numtriangles;
                    numtriangles++;
                }
            } 
            else 
            {
                /* v */
				//if (n== 181228)				
                sscanf(buf, "%d", &v);
                this->T(numtriangles).vindices[0] = v;
                //this->T(numtriangles).tindices[0] = 0;
                //this->T(numtriangles).nindices[0] = 0;

                fscanf(file, "%d", &v);
                this->T(numtriangles).vindices[1] = v;
                //this->T(numtriangles).tindices[1] = 0;
                //this->T(numtriangles).nindices[1] = 0;

                fscanf(file, "%d", &v);
                //this->T(numtriangles).vindices[2] = v;
                //this->T(numtriangles).tindices[2] = 0;
                //this->T(numtriangles).nindices[2] = 0;

                group->triangles[group->numtriangles++] = numtriangles;
                numtriangles++;
                while(fscanf(file, "%d", &v) > 0) 
                {
                    this->T(numtriangles).vindices[0] = this->T(numtriangles-1).vindices[0];
                    //this->T(numtriangles).tindices[0] = this->T(numtriangles-1).tindices[0];
                    //this->T(numtriangles).nindices[0] = this->T(numtriangles-1).nindices[0];

                    this->T(numtriangles).vindices[1] = this->T(numtriangles-1).vindices[2];
                    //this->T(numtriangles).tindices[1] = this->T(numtriangles-1).tindices[2];
                    //this->T(numtriangles).nindices[1] = this->T(numtriangles-1).nindices[2];

                    this->T(numtriangles).vindices[2] = v;
                    //this->T(numtriangles).tindices[2] = 0;
                    //this->T(numtriangles).nindices[2] = 0;

                    group->triangles[group->numtriangles++] = numtriangles;
                    numtriangles++;
                }
            }
            break;
                
            default:
                /* eat up rest of line */
                fgets(buf, sizeof(buf), file);
                break;
    }
  }
#if 0
  /* announce the memory requirements */
  printf(" Memory: %d bytes\n",
      numvertices  * 3*sizeof(GLfloat) +
      numnormals   * 3*sizeof(GLfloat) * (numnormals ? 1 : 0) +
      numtexcoords * 3*sizeof(GLfloat) * (numtexcoords ? 1 : 0) +
      numtriangles * sizeof(OBJtriangle));
#endif
}

void Model::ReadMTL(char* name)
{
    FILE* file;
    char* dir;
    char* filename;
    char* texture_name;
    char  buf[128];
    GLuint nummaterials, i;
	
    dir = DirName(pathname);
    filename = (char*)malloc(sizeof(char) * (strlen(dir) + strlen(name) + 1));
    strcpy(filename, dir);
    strcat(filename, name);
    free(dir);
    
    file = fopen(filename, "r");
    if (!file) 
    {
        fprintf(stderr, "ReadMTL() failed: can't open material file \"%s\".\n", filename);
        exit(1);
    }
    free(filename);
    
    /* count the number of materials in the file */
    nummaterials = 1;
    while(fscanf(file, "%s", buf) != EOF) 
    {
        switch(buf[0]) {
        case '#':               /* comment */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        case 'n':               /* newmtl */
            fgets(buf, sizeof(buf), file);
            nummaterials++;
            sscanf(buf, "%s %s", buf, buf);
            break;
        default:
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        }
    }
    
    rewind(file);
    
    this->materials = (OBJmaterial*)malloc(sizeof(OBJmaterial) * nummaterials);
    this->nummaterials = nummaterials;
    
    /* set the default material */
    for (i = 0; i < nummaterials; i++) {
        this->materials[i].name = NULL;
        this->materials[i].shininess = 65.0;
        this->materials[i].diffuse[0] = 0.8;
        this->materials[i].diffuse[1] = 0.8;
        this->materials[i].diffuse[2] = 0.8;
        this->materials[i].diffuse[3] = 1.0;
        this->materials[i].ambient[0] = 0.2;
        this->materials[i].ambient[1] = 0.2;
        this->materials[i].ambient[2] = 0.2;
        this->materials[i].ambient[3] = 1.0;
        this->materials[i].specular[0] = 0.0;
        this->materials[i].specular[1] = 0.0;
        this->materials[i].specular[2] = 0.0;
        this->materials[i].specular[3] = 1.0;
		this->materials[i].IDTextura = -1;
    }
    this->materials[0].name = strdup("default");
    
    /* now, read in the data */
    nummaterials = 0;
    while(fscanf(file, "%s", buf) != EOF) {
        switch(buf[0]) {
        case '#':               /* comment */
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        case 'n':               /* newmtl */
            fgets(buf, sizeof(buf), file);
            sscanf(buf, "%s %s", buf, buf);
            nummaterials++;
            this->materials[nummaterials].name = strdup(buf);
            break;
        case 'N':
            if (buf[1]!='s') break; // 3DS pune 'i' aici pentru indici de refractie si se incurca
            fscanf(file, "%f", &this->materials[nummaterials].shininess);
            /* wavefront shininess is from [0, 1000], so scale for OpenGL */
            this->materials[nummaterials].shininess /= 1000.0;
            this->materials[nummaterials].shininess *= 128.0;
            break;
        case 'K':
            switch(buf[1]) {
            case 'd':
                fscanf(file, "%f %f %f",
                    &this->materials[nummaterials].diffuse[0],
                    &this->materials[nummaterials].diffuse[1],
                    &this->materials[nummaterials].diffuse[2]);
                break;
            case 's':
                fscanf(file, "%f %f %f",
                    &this->materials[nummaterials].specular[0],
                    &this->materials[nummaterials].specular[1],
                    &this->materials[nummaterials].specular[2]);
                break;
            case 'a':
                fscanf(file, "%f %f %f",
                    &this->materials[nummaterials].ambient[0],
                    &this->materials[nummaterials].ambient[1],
                    &this->materials[nummaterials].ambient[2]);
                break;
            default:
                /* eat up rest of line */
                fgets(buf, sizeof(buf), file);
                break;
            }
            break;
        case 'm':
	        // harta de texturi
	        filename = (char *)malloc(FILENAME_MAX);
	        fgets(filename, FILENAME_MAX, file);
	        texture_name = strdup(filename);
	        free(filename);
	        if(strncmp(buf, "map_Kd", 6) == 0) 
            {
				char afis[180];
                sprintf(afis,"Loading Textures (%s)...",texture_name);
                this->materials[nummaterials].IDTextura = FindOrAddTexture(texture_name);

	            free(texture_name);
	        } 
	        else 
	        {
	            free(texture_name);
	        }
	        break;
        default:
            /* eat up rest of line */
            fgets(buf, sizeof(buf), file);
            break;
        }
    }
}

int Model::FindOrAddTexture(const char* name)
{
    GLuint i;
    char *dir, *filename;

    char *numefis = new char[strlen(name) + 1];
    strcpy(numefis, name);

    while (*numefis==' ') numefis++;

    for (i = 0; i < this->numtextures; i++) {
        if (!strcmp(this->textures[i].Get_name(), numefis))
            return i;
    }
	char afis[180];
	sprintf(afis,"Loading Textures (%s )...",name);
	
	if (strstr(name,":\\"))
	{
		filename = (char*)malloc(sizeof(char) * (strlen(name) + 1));
		strcpy(filename,name);		
	}
	else
	{
		dir = DirName(this->pathname);
		filename = (char*)malloc(sizeof(char) * (strlen(dir) + strlen(numefis) + 1));
	
		strcpy(filename, dir);
		strcat(filename, numefis);
		free(dir);
	}
    int lung = strlen(filename);
	if (filename[lung-1]<32) filename[lung-1]=0;
	if (filename[lung-2]<32) filename[lung-2]=0;    

    this->numtextures++;
    this->textures = (Texture*)realloc(this->textures, sizeof(Texture)*this->numtextures);
    this->textures[this->numtextures-1].Set_name(strdup(numefis));
    this->textures[this->numtextures-1].Load(filename);
    

    free(filename);

    return this->numtextures-1;
}

/* AddGroup: Add a group to the model */
OBJgroup* Model::AddGroup(char* name)
{
    OBJgroup* group;
    
    group = FindGroup(name);
    if (!group) {
        group = (OBJgroup*)malloc(sizeof(OBJgroup));
        group->name = strdup(name);
        group->material = 0;
        group->numtriangles = 0;
        group->triangles = NULL;
        group->next = groups;
        groups = group;
        numgroups++;
    }
    
    return group;
}

OBJgroup* Model::FindGroup(char* name)
{
    OBJgroup* group;
    
    group = this->groups;
    while(group) {
        if (!strcmp(name, group->name))			
            break;
        group = group->next;
    }
    
    return group;
}

char* Model::DirName(char* path)
{
    char* dir;
    char* s;
    
    dir = strdup(path);
    
    s = strrchr(dir, '/');
    if (s)
        s[1] = '\0';
    else
        dir[0] = '\0';
    
    return dir;
}

GLuint Model::FindMaterial(char* name)
{
    GLuint i;
    
    /* XXX doing a linear search on a string key'd list is pretty lame,
    but it works and is fast enough for now. */
    for (i = 0; i < this->nummaterials; i++) 
    {
        if (!strcmp(this->materials[i].name, name))
            return i;
    }
    
    /* didn't find the name, so print a warning and return the default
    material (0). */
    printf("FindMaterial():  can't find material \"%s\".\n", name);
    i = 0;
    return i;
}

GLvoid Model::Build()
{
    static GLuint i;
    static OBJgroup* group;
    static OBJtriangle* triangle;
    static OBJmaterial* material;
    GLuint IDTextura;

    GLuint mode = this->mode;
    
    assert(this->vertices);
    /* do a bit of warning */
    if (mode & OBJ_FLAT && !this->facetnorms) {
        printf("Draw() warning: flat render mode requested "
            "with no facet normals defined.\n");
        mode &= ~OBJ_FLAT;
    }
    if (mode & OBJ_SMOOTH && !this->normals) {
        printf("Draw() warning: smooth render mode requested "
            "with no normals defined.\n");
        mode &= ~OBJ_SMOOTH;
    }
    if (mode & OBJ_TEXTURE && !this->texcoords) {
        printf("Draw() warning: texture render mode requested "
            "with no texture coordinates defined.\n");
        mode &= ~OBJ_TEXTURE;
    }
    if (mode & OBJ_FLAT && mode & OBJ_SMOOTH) {
        printf("Draw() warning: flat render mode requested "
            "and smooth render mode requested (using smooth).\n");
        mode &= ~OBJ_FLAT;
    }
    if (mode & OBJ_COLOR && !this->materials) {
        printf("Draw() warning: color render mode requested "
            "with no materials defined.\n");
        mode &= ~OBJ_COLOR;
    }
    if (mode & OBJ_MATERIAL && !this->materials) {
        printf("Draw() warning: material render mode requested "
            "with no materials defined.\n");
        mode &= ~OBJ_MATERIAL;
    }
    if (mode & OBJ_COLOR && mode & OBJ_MATERIAL) {
        printf("Draw() warning: color and material render mode requested "
            "using only material mode.\n");
        mode &= ~OBJ_COLOR;
    }

    if (mode & OBJ_COLOR)
        glEnable(GL_COLOR_MATERIAL);
    else if (mode & OBJ_MATERIAL)
        glDisable(GL_COLOR_MATERIAL);

    if (mode & OBJ_TEXTURE) 
    {
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    }
    
    IDTextura = -1;
    group = this->groups;
    while (group) 
	{
        material = &(this->materials[group->material]);

		if (material)
			IDTextura = material->IDTextura;
		else 
			IDTextura=-1;
        
		if (mode & OBJ_MATERIAL) 
		{            
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material->ambient);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material->diffuse);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material->specular);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material->shininess);
        }

        if (mode & OBJ_TEXTURE)
		{				
			if(IDTextura == -1)
				glBindTexture(GL_TEXTURE_2D, 0);
			else
                glBindTexture(GL_TEXTURE_2D, this->textures[IDTextura].Get_id());
		}

        if (mode & OBJ_COLOR) {
            glColor3fv(material->diffuse);
        }

        glBegin(GL_TRIANGLES);
        for (i = 0; i < group->numtriangles; i++) {
            triangle = &T(group->triangles[i]);
#ifdef DebugVisibleSurfaces
            if (!triangle->visible) continue;
#endif
            if (mode & OBJ_FLAT)
                glNormal3fv(&this->facetnorms[3 * triangle->findex]);

            for(int j = 0; j < 3; j++)
            {
                if (mode & OBJ_SMOOTH)
                    glNormal3fv(&this->normals[3 * triangle->nindices[j]]);

                if (mode & OBJ_TEXTURE)
                    glTexCoord2fv(&this->texcoords[2 * triangle->tindices[j]]);

                glVertex3fv(&this->vertices[3 * triangle->vindices[j]]);
            }
        }
        glEnd();
        
        group = group->next;
    }
}

void Model::Draw()
{
    if(!Loaded)
        return;

	glPushMatrix();

    if ( listId_ != 0 )
    {
        glCallList( listId_ );
    }

    glPopMatrix();
}

void Model::setData()
{

    float temp[] = {minx, maxx, miny, maxy, minz, maxz};

    int idx = 0;

    for(int i = 0; i < 2; i++)
    for(int j = 2; j < 4; j++)
    for(int k = 4; k < 6; k++)
    {
        points[idx] = glm::vec3(temp[i], temp[j], temp[k]);
    }
}
