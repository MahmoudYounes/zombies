#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "Model.h"
#include "GameObject.h"

using namespace std;

class Zombie : public GameObject
{
  public:
    Zombie(Model* model ,glm::vec3 Pos, glm::vec3 player);
    glm::vec3 getPosition();
    void draw();
    void Move(btVector3);
    btRigidBody* CreateBody();
  private:
    glm::vec3 prev;
    float angle;
    float minx, maxx;
    float miny, maxy;
    float minz, maxz;
    Model *model;
};

#endif


