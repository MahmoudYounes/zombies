#include "Texture.h"

using namespace std;

Texture::Texture()
{
    name = NULL;
}

Texture::~Texture()
{
	if (id != 0)
        glDeleteTextures( 1, &id );

    if(name)
        delete[] name;
}

char* Texture::Get_name()
{
    return name;
}

void Texture::Set_name(char* name)
{
    this->name = new char[strlen(name) + 1];
    strcpy(this->name, name);
}

GLuint Texture::Get_id()
{
	return id;
}
GLuint Texture::Load(char* filename)
{
    // ---State preservation---
    GLuint previous_texture;
    glGetIntegerv( GL_TEXTURE_BINDING_2D, (GLint*)&previous_texture );

    // Generate a new texture id
    GLuint texture_id;
    glGenTextures( 1, &texture_id );

    // Load the texture using GLFW
    glBindTexture( GL_TEXTURE_2D, texture_id );
    int result = glfwLoadTexture2D( filename, GLFW_BUILD_MIPMAPS_BIT );

    if ( result==GL_FALSE )
    {
        // An error occured during loading
        std::cerr << "Warning: cannot load texture \"" << filename << "\". GLFW reported an error (GL_FALSE) with glfwLoadTexture2D." << std::endl;

        // ---State preservation---
        glBindTexture( GL_TEXTURE_2D, previous_texture );

        return false;
    }
    // Nice trilinear filtering.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    // ---State preservation---
    glBindTexture( GL_TEXTURE_2D, previous_texture );

    id = texture_id;

    return texture_id;
}
