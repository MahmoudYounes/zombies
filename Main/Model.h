#ifndef MODEL_H
#define MODEL_H

#include "Texture.h"
#include <glm/glm.hpp>

#ifndef M_PI
#define M_PI 3.14159265f
#endif

#define OBJ_NONE     (0)            /* render with only vertices */
#define OBJ_FLAT     (1 << 0)       /* render with facet normals */
#define OBJ_SMOOTH   (1 << 1)       /* render with vertex normals */
#define OBJ_TEXTURE  (1 << 2)       /* render with texture coords */
#define OBJ_COLOR    (1 << 3)       /* render with colors */
#define OBJ_MATERIAL (1 << 4)       /* render with materials */

typedef struct _OBJmaterial
{
  char* name;                   /* name of material */
  GLfloat diffuse[4];           /* diffuse component */
  GLfloat ambient[4];           /* ambient component */
  GLfloat specular[4];          /* specular component */
  GLfloat emmissive[4];         /* emmissive component */
  GLfloat shininess;            /* specular exponent */
//this is for textures
  GLuint IDTextura;		// ID-ul texturii difuze
} OBJmaterial;

/* OBJtriangle: Structure that defines a triangle in a model.
 */
typedef struct _OBJtriangle {
  GLuint vindices[3];           /* array of triangle vertex indices */
  GLuint nindices[3];           /* array of triangle normal indices */
  GLuint tindices[3];           /* array of triangle texcoord indices*/
  GLuint findex;                /* index of triangle facet normal */
  //GLuint nrvecini;
  GLuint vecini[3];
  bool visible;
} OBJtriangle;

//adaugat pentru suport texturi
typedef struct _OBJtexture {
  char *name;
  GLuint id;                    /* ID-ul texturii */
  GLfloat width;		/* width and height for texture coordinates */
  GLfloat height;
} OBJtexture;

/* OBJgroup: Structure that defines a group in a model.
 */
typedef struct _OBJgroup {
  char*             name;           /* name of this group */
  GLuint            numtriangles;   /* number of triangles in this group */
  GLuint*           triangles;      /* array of triangle indices */
  GLuint            material;       /* index to material for group */
  struct _OBJgroup* next;           /* pointer to next group in model */
} OBJgroup;

/* _GLMnode: general purpose node */
typedef struct _OBJnode {
    GLuint         index;
    GLboolean      averaged;
    struct _OBJnode* next;
} OBJnode;

///////////////////////////////////////////////////////////////////////
class Model
{
public:
    Model(char* = NULL, GLuint = 0);
    ~Model();
    void Load(char* filename, GLuint mode);
    void Draw();

    float minx, maxx;
    float miny, maxy;
    float minz, maxz;

    glm::vec3 points[8];

private:
    void FirstPass(FILE*);
    void SecondPass(FILE*);
    void ReadMTL(char* name);
    void Build();
    int  FindOrAddTexture(const char* name);
    char* DirName(char* path);
    OBJgroup* AddGroup(char* name);
    OBJgroup* FindGroup(char* name);
    GLuint FindMaterial(char* name);

	char*    pathname;            /* path to this model */
	char*    mtllibname;          /* name of the material library */

	GLuint   numvertices;         /* number of vertices in model */
	GLfloat* vertices;            /* array of vertices  */

	GLuint   numnormals;          /* number of normals in model */
	GLfloat* normals;             /* array of normals */

	GLuint   numtexcoords;        /* number of texcoords in model */
	GLfloat* texcoords;           /* array of texture coordinates */

	GLuint   numfacetnorms;       /* number of facetnorms in model */
	GLfloat* facetnorms;          /* array of facetnorms */

	GLuint       numtriangles;    /* number of triangles in model */
	OBJtriangle* triangles;       /* array of triangles */

	GLuint       nummaterials;    /* number of materials in model */
	OBJmaterial* materials;       /* array of materials */

	GLuint       numgroups;       /* number of groups in model */
	OBJgroup*    groups;          /* linked list of groups */

	// textures
    GLuint    numtextures;
	Texture*  textures;

	GLuint mode;
	GLuint listId_;

    void setData();
    bool Loaded;
};

#endif
