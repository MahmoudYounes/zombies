#ifndef CAMERA_H
#define CAMERA_H

#include "Texture.h"
#include "Skybox.h"
#include "Model.h"
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>

class Camera
{
    public:
        //Constructor by choosing the initial position and the angles of the camera
        Camera(glm::vec3 position, float angle_LR = 0.0f, float angle_UD = 0.0f, Skybox* skybox = NULL);

        //Move the camera forward
        void MoveForward(float distance);

        //Move the camera backwards
        void MoveBackward(float distance);

        void MoveLeft(float distance);

        void MoveRight(float distance);

        //return position of the camera
        const glm::vec3 Get_position();

        //return angle of rotation about Y-axis (Left-Right angle)
        float Get_LR_angle();

        //return angle of rotation about X-axis (Up-Down angle)
        float Get_UD_angle();

        //transforms the whole scene according to the position of the camera and the angles with the axiis
        void Process();

        void calculate_mouse();

        void reset();
        void Draw();

        glm::vec3 GetDirection();

    private:
        float mouseSpeed;
        double lastTime;
        glm::vec3 position;     //Position of the camera
        float angle_LeftRight;  //Angle with the y-axis
        float angle_UpDown;     //Angle with the x-axis

        Skybox* _skybox;
};

#endif
