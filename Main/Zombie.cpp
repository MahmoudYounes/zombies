#include "Zombie.h"
#include <iostream>
#include <iostream>
#include "Model.h"
#include "Texture.h"
#include "camera.h"
#include <stdlib.h>

using namespace std;

//___________________________________________________________________________
Zombie::Zombie(Model* model ,glm::vec3 Pos, glm::vec3 player)
{
    glm::vec3 temp1= player;
    temp1.y = 0;
    prev = temp1;
    temp1 = glm::normalize(temp1-Pos);
    float result = glm::dot(glm::vec3(0, 0, 1), temp1);
    angle = acos(result) * 180 / M_PI;

    position=Pos;
    this->model = model;
    body = NULL;
    minx = model->minx;
    maxx = model->maxx;
    miny = model->miny;
    maxy = model->maxy;
    minz = model->minz;
    maxz = model->maxz;
}
//___________________________________________________________________________
glm::vec3 Zombie::getPosition()
{
    return  position;
}
//___________________________________________________________________________
void Zombie::draw()
{
    glPushMatrix();
    btTransform t;
    //glTranslatef(position.x, position.y, position.z);
    body->getMotionState()->getWorldTransform(t);	//get the transform
    float mat[16];
    t.getOpenGLMatrix(mat);	//OpenGL matrix stores the rotation and orientation
    glMultMatrixf(mat);
    glRotatef(angle, 0, 1, 0);
    model->Draw();
    glPopMatrix();
}

btRigidBody* Zombie::CreateBody()
{
    GLfloat height = maxy - miny;
    GLfloat depth = maxz - minz;
    GLfloat width = maxx - minx;
    GLfloat mass = 2000.0;

    btTransform transform;

    transform.setIdentity();
    transform.setOrigin(btVector3(position.x,position.y,position.z));

    //btCapsuleShape* bla = new btCapsuleShape(radius, height);
    btBoxShape* box = new btBoxShape(btVector3(depth / 2, height / 2, width / 2));

    btVector3 inertia(0,0,0);
    //box->calculateLocalInertia(mass,inertia);

    btMotionState* motion = new btDefaultMotionState(transform);

    btRigidBody::btRigidBodyConstructionInfo info(mass, motion, box, inertia);
    btRigidBody* body = new btRigidBody(info);

    this->body = body;

    return body;
}

void Zombie::Move(btVector3 player)
{
//    glm::vec3 temp1(player.x(), 0, player.z());
//    if(temp1 == prev)
//        return;

//    prev = temp1;
//    glm::vec3 temp2 = glm::vec3(sin(angle * M_PI / 180), 0, cos(angle * M_PI / 180));

//    temp1 = glm::normalize(temp1 - position);
//    glm::vec3 temp3 = glm::cross(temp2, temp1);

//    float mag = sqrt(temp3.x * temp3.x + temp3.y * temp3.y + temp3.z * temp3.z);

//    float delta = asin(mag) / M_PI * 180;

//    if(temp3.y < 0)
//    {
//        angle -= delta;
//    }
//    else
//    {
//        angle += delta;
//    }

    btVector3 temp;
    btTransform T;
    body->activate();
    body->getMotionState()->getWorldTransform(T);
    temp = T.getOrigin();
    temp = player - temp;
    temp = temp.normalize();
    temp *= 20;
    temp[1]=0;
    body->setLinearVelocity(temp);
}
