#include "Model.h"
#include "Texture.h"
#include "camera.h"
#include "Skybox.h"
#include "GameObject.h"
#include "Buildings.h"
#include "Zombie.h"
#include <list>
#include <map>


static const unsigned int width  = 1920;
static const unsigned int height = 1080;

#include <vector>
#include <btBulletDynamicsCommon.h>	//you may need to change this

GLUquadricObj* quad;
btDynamicsWorld* world;	//every physical object go to the world
btDispatcher* dispatcher;	//what collision algorithm to use?
btCollisionConfiguration* collisionConfig;	//what collision algorithm to use?
btBroadphaseInterface* broadphase;	//should Bullet examine every object, or just what close to each other
btConstraintSolver* solver;					//solve collisions, apply forces, impulses
std::list<btRigidBody*> bodies;
btRigidBody* ground;

std::map<std::string, Model*> models;

void light()
{
    glEnable(GL_LIGHTING); glEnable(GL_LIGHT0);
    // Create light components
    GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
    GLfloat specularLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat position[] = { 100.0f, 100.0f, 100.0f, 1.0f };
    // Assign created components to GL_LIGHT0
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position);

}

void createModels()
{
    models["building1"] = new Model("models/Buildings/Building1/FarmhouseOBJ.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    models["building2"] = new Model("models/Buildings/Building2/Building2.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    models["building3"] = new Model("models/Buildings/Building3/Stable.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    models["zombie_f" ] = new Model("models/Lambent_Female/Lambent_Female.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    models["zombie_m" ] = new Model("models/Lambent_Male/Lambent_Male.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);

}
void DrawWithTrans(Model& _obj,glm::vec3 pos, glm::vec3 scale){
    glPushMatrix();
    glTranslatef(pos.x,pos.y,pos.z);
    glScalef(scale.x, scale.y, scale.z);
    _obj.Draw();
    glPopMatrix();
}

btRigidBody* addBox(float width, float height, float depth, float x, float y, float z, float mass)
{
    btTransform transform;

    transform.setIdentity();
    transform.setOrigin(btVector3(x,y,z));

    btBoxShape* box = new btBoxShape(btVector3(width/2.0, height/2.0, depth/2.0));
    btVector3 inertia(0,0,0);

    if(mass != 0) // dynamic object
        box->calculateLocalInertia(mass,inertia);

    btMotionState* motion = new btDefaultMotionState(transform);

    btRigidBody::btRigidBodyConstructionInfo info(mass, motion, box, inertia);
    btRigidBody* body = new btRigidBody(info);

    world->addRigidBody(body);
    bodies.push_back(body);

    return body;
}

btRigidBody* addSphere(float rad,float x,float y,float z,float mass)
{
	btTransform t;	//position and rotation
	t.setIdentity();
	t.setOrigin(btVector3(x,y,z));	//put it to x,y,z coordinates
	btSphereShape* sphere=new btSphereShape(rad);	//it's a sphere, so use sphereshape
	btVector3 inertia(0,0,0);	//inertia is 0,0,0 for static object, else
	if(mass!=0.0)
		sphere->calculateLocalInertia(mass,inertia);	//it can be determined by this function (for all kind of shapes)

	btMotionState* motion=new btDefaultMotionState(t);	//set the position (and motion)
	btRigidBody::btRigidBodyConstructionInfo info(mass,motion,sphere,inertia);	//create the constructioninfo, you can create multiple bodies with the same info
	btRigidBody* body=new btRigidBody(info);	//let's create the body itself
	world->addRigidBody(body);	//and let the world know about it
	bodies.push_back(body);	//to be easier to clean, I store them a vector
	return body;
}
Model bullet;

void renderSphere(btRigidBody* sphere)
{
	if(sphere->getCollisionShape()->getShapeType()!=SPHERE_SHAPE_PROXYTYPE)	//only render, if it's a sphere
		return;
	glColor3f(0.3f,0.3f,0);
	float r=((btSphereShape*)sphere->getCollisionShape())->getRadius();
	btTransform t;
	sphere->getMotionState()->getWorldTransform(t);	//get the transform
	float mat[16];
	t.getOpenGLMatrix(mat);	//OpenGL matrix stores the rotation and orientation
	glPushMatrix();
		glMultMatrixf(mat);	//multiplying the current matrix with it moves the object in place
		gluSphere(quad,r*.5,20,20);
		//bullet.Draw();
	glPopMatrix();
}

//similar then renderSphere function
void renderPlane(btRigidBody* plane)
{
	if(plane->getCollisionShape()->getShapeType()!=STATIC_PLANE_PROXYTYPE)
		return;
	glColor3f(0.8,0.8,0.8);
	btTransform t;
	plane->getMotionState()->getWorldTransform(t);
	float mat[16];
	t.getOpenGLMatrix(mat);
	glPushMatrix();
		glMultMatrixf(mat);	//translation,rotation
		glBegin(GL_QUADS);
			glVertex3f(-1000,0,1000);
			glVertex3f(-1000,0,-1000);
			glVertex3f(1000,0,-1000);
			glVertex3f(1000,0,1000);
		glEnd();
	glPopMatrix();
}

void init()
{
    glfwInit();

    //glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4); // fullscreen antialiasing
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 2);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 1);
    glfwOpenWindow(width, height, 8, 8, 8, 8, 24, 0, GLFW_FULLSCREEN);
    glfwSetWindowTitle("test");
    glfwSwapInterval(1); // vsync enabled

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glEnable(GL_DEPTH_TEST);

    glShadeModel(GL_SMOOTH);
    //glEnable(GL_CULL_FACE);

    glClearColor(0, 0, 0, 1);

    //pretty much initialize everything logically
	collisionConfig=new btDefaultCollisionConfiguration();
	dispatcher=new btCollisionDispatcher(collisionConfig);
	broadphase=new btDbvtBroadphase();
	solver=new btSequentialImpulseConstraintSolver();
	world=new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConfig);
	world->setGravity(btVector3(0,-10,0));	//gravity on Earth

    //similar to createSphere
    btTransform t;
    t.setIdentity();
    t.setOrigin(btVector3(0,0,0));
    btStaticPlaneShape* plane=new btStaticPlaneShape(btVector3(0,1,0),0);
    btMotionState* motion=new btDefaultMotionState(t);
    btRigidBody::btRigidBodyConstructionInfo info(0.0,motion,plane);
    ground=new btRigidBody(info);
    world->addRigidBody(ground);
    //bodies.push_back(body);

	quad=gluNewQuadric();
}

void setup_normal_view()
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)width/(float)height, 1, 2001.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void release_glfw()
{
  glfwTerminate();
}

void drawHealth(float health) {
  const int numDiv = 10;
  const float sep = 0.04;
  const float barHeight = 0.5/(float)numDiv;
  glBegin(GL_QUADS);
    glColor3f(1, 0, 0);
    for(float i = 0; i < health/2; i += (sep + barHeight)) {
      glVertex2f(-2.2, i-0.8);
      glVertex2f(-2, i-0.8);
      glVertex2f(-2, i + barHeight-0.8);
      glVertex2f(-2.2, i + barHeight-0.8);
    }
  glEnd();
}

void Sprint( int x, int y, char *st)
{
    int l,i;
    l=strlen( st ); // see how many characters are in text string.
    glRasterPos2i( x, y); // location to start printing text
    for( i=0; i < l; i++) // loop until i is greater then l
    {
//        glutBitmapCharacter(1, st[i]); // Print a character on the screen
    }

}



float playerHealth = 1;

int main(int , char**)
{
    init();
    vector <Zombie*> zombies;
    Model test11, test12, test13, player, gun;
    //bullet.Load("models/bullet2/Bullet.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    player.Load("models/Player/fighter.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    gun.Load("models/GunScale/gunScale.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);

    Skybox skybox("models/skybox/top.tga",
                  "models/skybox/down.tga",
                  "models/skybox/front.tga",
                  "models/skybox/back.tga",
                  "models/skybox/left.tga",
                  "models/skybox/right.tga");

    Camera camera(glm::vec3(35,6,30), 0, 0, &skybox);

    std::vector<GameObject*> objects;
    createModels();

    test11.Load("models/Plane/asphalt.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);
    test12.Load("models/wall/wall.obj", OBJ_SMOOTH | OBJ_TEXTURE | OBJ_COLOR);

    int add=75,add2=75;
    for(int count=0,facZ=-150;count<=5;count++,facZ+=add2){
        int facM=-150;

        for (int i=0;i<2;i++,facM+=add)
        {
            objects.push_back(new Building(glm::vec3(facM,0,facZ), models["building1"]));
            world->addRigidBody(objects.back()->CreateBody());
        }

        for (int i=0;i<2;i++,facM+=add)
        {
            objects.push_back(new Building(glm::vec3(facM,0,facZ), models["building2"]));
            world->addRigidBody(objects.back()->CreateBody());
        }

        for (int i=0;i<2;i++,facM+=add)
        {
            objects.push_back(new Building(glm::vec3(facM,0,facZ), models["building3"]));
            world->addRigidBody(objects.back()->CreateBody());
        }
    }
    zombies.push_back(new Zombie(models["zombie_m"], glm::vec3(30, 0, 10), camera.Get_position()));
    world->addRigidBody(zombies.back()->CreateBody());
    zombies.push_back(new Zombie(models["zombie_f"], glm::vec3(30, 0, 0 ), camera.Get_position() ));
    world->addRigidBody(zombies.back()->CreateBody());


    int px[200];
    int pz[200];
    for (int i=0; i<200; i++)
    {
        px[i]=rand()%500 -250;
        pz[i]=rand()%500 -250;
    }

    for (int i=0;i<200;i+=2)
    {
        zombies.push_back(new Zombie(models["zombie_m"], glm::vec3(px[i]    , 0, pz[i]    ), camera.Get_position()));
        world->addRigidBody(zombies.back()->CreateBody());
        zombies.push_back(new Zombie(models["zombie_f"], glm::vec3(px[i + 1], 0, pz[i + 1]), camera.Get_position()));
        world->addRigidBody(zombies.back()->CreateBody());
    }
    float walkTranslation = 0;
    while(1)
    {
        if (zombies.size()==0)
            {
                cout<<"You WIN!!\n";
                break;
            }
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (glfwGetKey(GLFW_KEY_ESC) || !glfwGetWindowParam(GLFW_OPENED))
              break;

        for(int z = 0; z < zombies.size(); z++)
        {
            for(list<btRigidBody*>::iterator i=bodies.begin();i!=bodies.end();i++)
            {
                    btTransform t, tz;
                    (*i)->getMotionState()->getWorldTransform(t);	//get the transform
                    btVector3 bulletPos = t.getOrigin();
                    zombies[z]->body->getMotionState()->getWorldTransform(tz);
                    btVector3 zombiePos = tz.getOrigin();
                    float dist = (zombiePos[0] - bulletPos[0])*(zombiePos[0] - bulletPos[0]) + (zombiePos[2] - bulletPos[2])*(zombiePos[2] - bulletPos[2]);
                    if (dist < 5)
                    {
                        delete zombies[z];
                        vector<Zombie*>::iterator zit = zombies.begin() + z;
                        zombies.erase(zit);
                        (*i)->setLinearVelocity(btVector3(0, 0, 0));
                    }
            }
            {
                if (zombies[z])
                {
                    btTransform tz;
                    zombies[z]->body->getMotionState()->getWorldTransform(tz);
                    btVector3 zombiePos=tz.getOrigin();
                    glm::vec3 CamPos=camera.Get_position();
                    float dist = (zombiePos[0] - CamPos[0])*(zombiePos[0] - CamPos[0]) + (zombiePos[2] - CamPos[2])*(zombiePos[2] - CamPos[2]);
                    if (dist<2)
                    {
                        playerHealth-=0.01;
                    }
                    if (playerHealth<=0)
                    {
                        cout<<"You LOSE\n";
                     exit(0);
                        //Sprint((width/2)-20,(height/2)-10,"YOU LOST");
                    }
                }

            }
        }

        setup_normal_view();

        double deltaTime = glfwGetTime()*100;
        glfwSetTime(0);

        if (glfwGetKey('W'))
        {
            camera.MoveForward(deltaTime / 5);
            walkTranslation += 0.2;
        }
        else if (glfwGetKey('S'))
            camera.MoveBackward(deltaTime / 5);
        else
        {
            walkTranslation = 0;
        }

        if (glfwGetKey('D'))
            camera.MoveRight(deltaTime / 5);
        else if (glfwGetKey('A'))
            camera.MoveLeft(deltaTime / 5);

        if(glfwGetKey(GLFW_KEY_ENTER))
        {
            glm::vec3 t = camera.Get_position();
            std::cout << t.x << ' ' << t.y << ' ' << t.z << std::endl;
        }

        if (glfwGetMouseButton(0))
        {
            if(bodies.size() >= 100)
            {
                btRigidBody* temp = bodies.front();
                world->removeCollisionObject(temp);
                btMotionState* motionState=(temp)->getMotionState();
                btCollisionShape* shape=(temp)->getCollisionShape();
                delete (temp);
                delete shape;
                delete motionState;

                bodies.pop_front();
            }

            glm::vec3 camPos = camera.Get_position();
            glm::vec3 direct = camera.GetDirection();

            btRigidBody* sphere=addSphere(0.1,camPos.x + direct.x*5,camPos.y + direct.y*5,camPos.z+direct.z*5,50.0);

            direct *= 300;
            sphere->setLinearVelocity(btVector3(direct.x, direct.y, direct.z));
        }

        glPushMatrix();
        glScalef(2,2,2);
        glTranslatef(0, -.5 + 0.1,-2.4);

        glPushMatrix();
        glTranslatef(0, 0.1*sin(walkTranslation), 0);
        player.Draw();

        glPopMatrix();
        drawHealth(playerHealth);
        glColor3f(0, 0, 0);
        gun.Draw();
        glPopMatrix();

        world->stepSimulation(1/30.0);
        camera.Process();

        light();
        //Plane
        glPushMatrix();
        glTranslatef(0, -1, 0);
        DrawWithTrans(test11,glm::vec3(0,1,0),glm::vec3(3.5,3.5,3.5));
        glPopMatrix();

        //Model *toDraw=&test;
        for(int i = 0; i < objects.size(); i++)
        {
            objects[i]->draw();
        }
        glm::vec3 pos = camera.Get_position();
        btVector3 playerPos = btVector3(pos.x, pos.y, pos.z);
        for(int i = 0; i < zombies.size(); i++)
        {
            zombies[i]->draw();
            zombies[i]->Move(playerPos);
        }
        for(list<btRigidBody*>::iterator i=bodies.begin();i!=bodies.end();i++)
        {
                renderSphere(*i);
        }

        //Walls
        glScalef(1, 0.5, 27);
        DrawWithTrans(test12,glm::vec3(-262,0,0),glm::vec3(1,1,1));
        DrawWithTrans(test12,glm::vec3( 262,0,0),glm::vec3(1,1,1));
        glScalef(1, 1, 1.0/27);


        glRotatef(90, 0, 1, 0);
        glScalef(1, 1, 27);
        DrawWithTrans(test12,glm::vec3(-262,0,0),glm::vec3(1,1,1));
        DrawWithTrans(test12,glm::vec3( 262,0,0),glm::vec3(1,1,1));

        glfwSwapBuffers();
    }

    //killskybox();
    for(list<btRigidBody*>::iterator i=bodies.begin();i!=bodies.end();i++)
    {
        world->removeCollisionObject(*i);
        btMotionState* motionState=(*i)->getMotionState();
        btCollisionShape* shape=(*i)->getCollisionShape();
        delete (*i);
        delete shape;
        delete motionState;
    }

    for(int i = 0; i < objects.size(); i++)
    {
        btRigidBody* temp = objects[i]->body;
        world->removeRigidBody(temp);

        btMotionState* motionState=temp->getMotionState();
        btCollisionShape* shape=temp->getCollisionShape();
        delete temp;
        delete shape;
        delete motionState;
    }
    for(int i = 0; i < zombies.size(); i++)
    {
        btRigidBody* temp = zombies[i]->body;
        world->removeRigidBody(temp);

        btMotionState* motionState=temp->getMotionState();
        btCollisionShape* shape=temp->getCollisionShape();
        delete temp;
        delete shape;
        delete motionState;
    }

    for(std::map<std::string,Model*>::iterator i=models.begin(); i!=models.end(); i++)
        delete (*i).second;

    world->removeCollisionObject(ground);
    btMotionState* motionState=(ground)->getMotionState();
    btCollisionShape* shape=(ground)->getCollisionShape();
    delete (ground);
    delete shape;
    delete motionState;

    delete dispatcher;
    delete collisionConfig;
    delete solver;
    delete broadphase;
    delete world;

    gluDeleteQuadric(quad);
    release_glfw();
    return 0;
}
