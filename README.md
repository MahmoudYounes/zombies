# README #

this is a game implemented with openGL under C++. the idea of this game revolves around an island full of zombies and you try to kill them all.

### How to set up and run? ###

* on linux:
* $ cmake CMakelist.txt
* $ make
* $ ./main

### Contribution guidelines ###

* contributors:
*     - mohamed abbas
*     - omar ahmad
*     - ali hussien
*     - mahmoud younes

### Who do I talk to? ###

* for any issues, mail: m.younesbadr@gmail.com